﻿using System;
using System.Linq;
using Calculation.Interfaces;

namespace Calculation.Implementation
{
	public class Calculator : BaseCalculator, ICalculator
	{
		private static ILogger _logger;
		public Calculator(ILogger logger)
		{
			_logger = logger;
		}

		public int Sum(params int[] numbers)
		{
			_logger.Trace(nameof(Calculator.Sum) + string.Join(",", numbers));
			try
            {
				_logger.Trace(nameof(Sum));
				var sum = SafeSum(numbers);
				_logger.Info(nameof(Sum) + string.Join(",", numbers) + sum.ToString());
                return sum;
			}
			catch (Exception e)
            {
				_logger.Error(e);
                throw;
            }
		}

		public int Sub(int a, int b)
		{
            try
            {
				return SafeSub(a, b);
			}
			catch (OverflowException e)
            {
				throw new OverflowException("The calculated number is outside of Int");
            }
			
		}

		public int Multiply(params int[] numbers)
		{
			if (!numbers.Any())
				return 0;

            try
            {
				return SafeMultiply(numbers);
			}
			catch (Exception e)
            {
				throw new InvalidOperationException("The operation not valid", e);
            }
			
		}

		public int Div(int a, int b)
		{
            try
            {
				return a / b;
			}
            catch
            {
				throw new InvalidOperationException("The operation not valid");
            }
		}
	}
}