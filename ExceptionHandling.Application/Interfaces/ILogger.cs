﻿using System;
using NLog;

namespace Calculation.Interfaces
{
	public interface ILogger
	{
		private static Logger _logger = LogManager.GetCurrentClassLogger();

		void Error(Exception ex)
        {
            _logger.Error(ex);
        }
		void Info(string message)
        {
            _logger.Info(message);
        }
		void Trace(string message)
        {
            _logger.Trace(message);
        }
	}
}